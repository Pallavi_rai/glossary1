<img src="images/IDSNlogo.png" width="100"><br>

<div align="center"><b>Introduction to Containers, Kubernetes, and OpenShift</b>
</div>

<div align="center">Module 2 Cheat Sheet
Understanding Kubernetes Architecture
</div>

<div align="center">The kubectl CLI
</div>

<table>
<tr>
<th width="30%">Command</th width="70%"><th>Description</th>
</tr>

<tr>
<td width="30%"><b>for …do</b></td>
<td width="70%">Runs a for command multiple times as specified.
</tr>

<tr>
<td width="30%"><b>kubectl apply </b></td>
<td width="70%">Applies a configuration to a resource.
</tr>


<tr>
<td width="30%"valign="top"><b>kubectl config get-clusters </b></td>
<td width="70%">Displays clusters defined in the kubeconfig.
</td>

</tr>

<tr>
<td width="30%"valign="top"><b>kubectl config get-contexts </b></td>
<td width="70%">Displays the current context.
</td>
</tr>


<tr>
<td width="30%"valign="top"><b>kubectl create
</b></td>
<td width="70%">Creates a resource.
</td>
</tr>


<tr>
<td width="30%"valign="top"><b>kubectl delete</b></td>
<td width="70%">Deletes resources.
</td>
</tr>


<tr>
<td width="30%"valign="top"><b>kubectl describe </b></td>
<td width="70%">Shows details of a resource or group of resources.
</td>
</tr>


<tr>
<td width="30%"valign="top"><b>kubectl expose</b></td>
<td width="70%">Exposes a resource to the internet as a Kubernetes service.
</td>
</tr>

<tr>
<td width="30%"valign="top"><b>kubectl get</b></td>
<td width="70%">Displays resources.
</td>
</tr>

<tr>
<td width="30%"valign="top"><b>kubectl get pods</b></td>
<td width="70%">Lists all the Pods.
</td>
</tr>

<tr>
<td width="30%"valign="top"><b>kubectl get pods -o wide</b></td>
<td width="70%">Lists all the Pods with details.
</td>
</tr>

<tr>
<td width="30%"valign="top"><b>kubectl get deployments</b></td>
<td width="70%">Lists the deployments created.
</td>
</tr>

<tr>
<td width="30%"valign="top"><b>kubectl get services</b></td>
<td width="70%">Lists the services created.
</td>
</tr>

<tr>
<td width="30%"valign="top"><b>kubectl proxy</b></td>
<td width="70%">Creates a proxy server between a localhost and the Kubernetes API server.
</td>
</tr>


<tr>
<td width="30%"valign="top"><b>kubectl run</b></td>
<td width="70%">Creates and runs a particular image in a pod.
</td>
</tr>

<tr>
<td width="30%"valign="top"><b>kubectl version</b></td>
<td width="70%">Prints the client and server version information.
</td>
</tr>


</table>




