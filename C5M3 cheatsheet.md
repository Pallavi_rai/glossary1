<img src="images/IDSNlogo.png" width="100"><br>

<div align="center"><b>Introduction to Containers, Kubernetes, and OpenShift</b>
</div>

<div align="center">Module 3 Cheat Sheet:
Managing Applications with Kubernetes
</div>

</div>

<div align="center">The kubectl CLI
</div>

<table>
<tr>
<th width="30%">Command</th width="70%"><th>Description</th>
</tr>

<tr>
<td width="30%"><b>kubectl autoscale deployment</b></td>
<td width="70%">Autoscales a Kubernetes Deployment.
</tr>

<tr>
<td width="30%"><b>kubectl create configmap</b></td>
<td width="70%">Creates a ConfigMap resource.
</tr>


<tr>
<td width="30%"valign="top"><b>kubectl get deployments -o wide</b></td>
<td width="70%">Lists deployments with details.
</td>

</tr>

<tr>
<td width="30%"valign="top"><b>kubectl get hpa</b></td>
<td width="70%">Lists Horizontal Pod Autoscalers (hpa)
</td>
</tr>


<tr>
<td width="30%"valign="top"><b>kubectl scale deployment
</b></td>
<td width="70%">Scales a deployment. 
</td>
</tr>


<tr>
<td width="30%"valign="top"><b>kubectl set image deployment</b></td>
<td width="70%">Updates the current deployment.
</td>
</tr>


<tr>
<td width="30%"valign="top"><b>kubectl rollout</b></td>
<td width="70%">Manages the rollout of a resource.
</td>
</tr>


<tr>
<td width="30%"valign="top"><b>kubectl rollout restart</b></td>
<td width="70%">Restarts the resource so that the containers restart.
</td>
</tr>

<tr>
<td width="30%"valign="top"><b>kubectl rollout undo</b></td>
<td width="70%">Rollbacks the resource.
</td>
</tr>


</table>




