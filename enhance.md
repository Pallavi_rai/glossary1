<link href="https://sk2280913.gitlab.io/Project-Management-Communication-Stakeholders-Leadership/style.css" rel="stylesheet"></link>                                                                                                              
<div style ="margin:50px;"></p>
<img src="../../images/SKTedtech_360%201.png" width="150"/>
                                                                                                             
<h1>Hands-On Lab: Assess Your Communication Style</h1>

<h4>Estimated duration: 15 minutes</h4>

<h2> Objective</h2>
<ul>
    <li>Identify your dominant communication style</li>
 </ul>

<h2> Prerequisites</h2>
<ol>
<li>You must have Microsoft Excel installed on your computer or a Microsoft Office 365 account online. If you want to sign up for Microsoft Office 365 online and need help with the steps to sign up, view the <a href="https://sk2280913.gitlab.io/Fundamentals-of-Office-Productivity-Software/labs/Sign_up_for_Microsoft_365.md.html" target = "_blank" rel="noopener noreferrer">Sign-up for Microsoft 365</a> lab.</li></p>
<li>If you are new to Microsoft Excel, view the <a href="https://sk2280913.gitlab.io/Using-Spreadsheet-Applications/labs/Fundamentals_of_Excel_Online.md.html" target = "_blank" rel="noopener noreferrer">Fundamentals of Excel Online</a> lab.</li></p>
</ol>
</p>

## Exercise: Identify your dominant communication style
<ul>
<b>Step 1</b>: Keeping the CTRL key pressed, click <a href="Assess_Your_Style.xlsx"> <b>here</b></a> to download the <b>Communication Style Survey</b>.</p>
<b>Note</b>: Use the COMMAND key instead of CTRL on Mac systems.</p>
<b>Step 2</b>: Open the document. There are five worksheets. The first four worksheets, Analytical, Structural, Conceptual, and Social, each contain a set of statements about the four different communication styles. </p>
  <img src="comm_styles.png" width="700"/></p>
 <b>Step 3</b>: Start with the statements in the <b>Analytical</b> worksheet. Read each statement and rate yourself on a scale of 1 to 5, 1 being Strongly Disagree and 5 being Strongly Agree. Place an <b>X</b> in the box that best represents your score.</p>
  <img src="comm_style1.png" width="800"/></p>
  
  <b>Step 4</b>: Next, click the <b>Structural</b> worksheet. Rate yourself for each statement in this worksheet. Place an <b>X</b> in the box that best represents your score.</p>
  <b>Step 5</b>: Next, click the <b>Conceptual</b> worksheet. Rate yourself for each statement in this worksheet. Place an <b>X</b> in the box that best represents your score.</p>
<b>Step 6</b>: Next, click the <b>Social</b> worksheet. Rate yourself for each statement in this worksheet. Place an <b>X</b> in the box that best represents your score.</p>
<b>Step 7</b>: After rating yourself for each communication style, click the <b>Results</b> worksheet. </p>
 <img src="results.png" width="300"/></p>
  <li>Notice that your score for each communication style along with your dominant style is displayed.</li></p>
  <li>If two scores are the same, consider both to be your dominant style. However, in the document, the style associated with the first top score is indicated as the dominant style.</li></p>
  <li>The table summarizes some tips on how to effectively communicate with individuals with different communication styles.</li></p>
 <table>
    <tr>
      <td><strong>Communication Style</strong></td>
      <td><strong>Tips for Effective Communication</strong></td>
    </tr>
    <tr>
        <td>Analytical</td>
      <td>
        <ul>
          <li>Keep your communications short—get to the point quickly.</li></p>
          <li>Beware of hijacking—state your what, why, who criteria up-front.</li></p>
          <li>Avoid drama, emotion, and surprises—focus on solving the problem.</li></p>
          <li>Strive for communications that are accurate, brief, and concise.</li></p>
          <li>Key on value. Show the return on investment.</li></p>
          <li>Expect some impatience, and judgmental language—don\’t be intimidated.</li></p>
          <li>If you don\’t know—don\’t try to bluff your way through.</li></p>
          <li>Have supporting information in your back pocket—ensure you have the facts.</li></p>
       </ul>
      </td>
    </tr>
    <tr>
        <td>Structural</td>
      <td>
        <ul>
          <li>Have an agenda—be organized—stay within the rules and guidelines.</li></p>
          <li>Provide read-ahead copies prior to presentation.</li></p>
          <li>Have details available if needed.</li></p>
          <li>Key on predictability and results.</li></p>
          <li>Avoid <b>out-of-the-box</b> what-if scenarios.</li></p>
          <li>Key on how, when, where questions—show who will be in control.</li></p>
          <li>Consider a step-by-step process-oriented approach –show how to get it done.</li></p>
          <li>Minimize change and surprises whenever possible.</li></p>
       </ul>
      </td>
    </tr>
  <tr>
       <td>Conceptual</td>
      <td>
        <ul>
          <li>Allow time for brainstorming and open-ended questions.</li></p>
          <li>Be ready to discuss the possibilities—what could be.</li></p>
          <li>Show the big picture—use visual depictions to your advantage.</li></p>
          <li>Metaphors work—expect abstract thinking—be prepared.</li></p>
          <li>Don\'t jump into the details unless necessary.</li></p>
          <li>Have all options available to discuss.</li></p>
          <li>Try to manage the tangents—conversation will shift quickly.</li></p>
          <li>Show numbers and facts graphically.</li></p>
       </ul>
      </td>
    </tr>
  <tr>
       <td>Social</td>
      <td>
        <ul>
          <li>Show how an idea will affect others.</li></p>
          <li>Share who is driving the idea and who is on the team.</li></p>
          <li>Ensure you make eye contact and watch your non-verbal cues.</li></p>
          <li>Personalize information as much as possible.</li></p>
          <li>Give opportunity for others to ask questions and respond—avoid interrupting.</li></p>
          <li>Use of stories and parables can be effective.</li></p>
          <li>Introduce yourself and your team—try to establish a relationship when possible.</li></p>
          <li>Show passion and feeling for the conversation.</li></p>
       </ul>
      </td>
    </tr>
</table>
 </ul>  
<b>Make sure to attempt the exercise, as it will help you gain a clear understanding of the concepts covered in the module.</b></p>
</p>


</div>

