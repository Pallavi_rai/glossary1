<img src="images/IDSNlogo.png" width="100"><br>

<div align="center"><b>Introduction to Containers, Kubernetes, and OpenShift</b>
</div>

<div align="center">Module 1 Glossary: Understanding the Benefits of Containers
</div>

<div align="center">The oc CLI
</div>

<table>
<tr>
<th width="30%">Command</th width="70%"><th>Description</th>
</tr>

<tr>
<td width="30%"><b>oc get</b></td>
<td width="70%">Displays a resource.
</tr>


<tr>
<td width="30%"valign="top"><b>oc project</b></td>
<td width="70%">Switches to a different project.
</td>

</tr>

<tr>
<td width="30%"valign="top"><b>oc version</b></td>
<td width="70%">Displays version information
</td>
</tr>


</table>




